package main

import (
    "bytes"
    "fmt"
    "io"
    "bufio"
    "net/http"
    "net/http/httputil"
    "log"
    "net"
    "os"
    "path"
)

var n uint64;  // timebomb

func recording_proxy(
    target *string,
    workdir *string,
    conn_in net.Conn,
) error {

    // read incoming http request
    req, err := http.ReadRequest(bufio.NewReader(conn_in))
    if err != nil { log.Fatal(err) }
    req_bytes, err := httputil.DumpRequest(req, true)
    if err != nil { log.Fatal(err) }

    n = n + 1
    log.Printf("(%d)\n", n)

    // defer
    defer conn_in.Close()

    // target connection
    conn_out, err := net.Dial("tcp", *target)
    if err != nil { log.Fatal(err) }

    // more defer
    defer conn_out.Close()

    // declare paths for record
    base := fmt.Sprintf("%s/%d", *workdir, n)
    err = os.Mkdir(base, 0755)
    if err != nil { log.Fatal(err) }
    req_fh,  err := os.Create(path.Join(base, "request"))
    if err != nil { log.Fatal(err) }
    resp_fh, err := os.Create(path.Join(base, "response"))
    if err != nil { log.Fatal(err) }

    // defers
    defer req_fh.Close()
    defer resp_fh.Close()

    // pass request to target and response back to client 
    n_req, err := io.Copy(conn_out, io.TeeReader(bytes.NewReader(req_bytes), req_fh))
    if err != nil { log.Fatal(err) }
    log.Printf("-> (%d)\n", n_req)
    n_resp, err := io.Copy(conn_in, io.TeeReader(conn_out, resp_fh))
    switch err {
    case nil:
    case io.EOF:
        log.Printf("<- (%d)\n", n_resp)
    default:
        log.Fatal(err)
    }

    return nil
}

func main() {
    log.SetFlags(log.Ltime | log.Lshortfile)

    target, ok := os.LookupEnv("TARGET_HOST")
    if !ok { log.Fatal("set TARGET_HOST") }
    listen, ok := os.LookupEnv("LISTEN")
    if !ok { panic("set LISTEN") }
    workdir, ok := os.LookupEnv("WORKDIR")
    if !ok { log.Fatal("set WORKDIR") }

    server, server_err := net.Listen("tcp", listen)
    log.Printf("%s -> %s (%s)", listen, target, workdir)
    if server_err != nil { log.Fatal(server_err) }

    for {
        client_conn, err := server.Accept()
        if err != nil { log.Fatal(err) }
        go recording_proxy(&target, &workdir, client_conn)
        log.Println("ok")
    }
}
