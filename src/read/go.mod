module read

go 1.13

require (
	github.com/beevik/etree v1.1.0
	golang.org/x/text v0.3.2
)
