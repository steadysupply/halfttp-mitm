package main

import (
    "bufio"
    "bytes"
    "io"
    "io/ioutil"
    "log"
    "net"
    "net/http"
    "os"
    "path"
    "sync"

    "golang.org/x/text/encoding/charmap"
    "github.com/beevik/etree"
)

var workdir string
var mutex = &sync.Mutex{}
var requests  map[string]*etree.Document
const MESSAGE_ID = "messageId"

func get_response_bytes(index_name string, message_id string) ([]byte, error) {
    // responses are advertised as being encoded as UTF-8, but are actually
    // encoded as ISO 8859-1, we decode as such (since go uses UTF-8
    // internally), replace message identifier to match incoming message id,
    // then re-encode as ISO 8895-1
    response_file, err := os.Open(path.Join(workdir, index_name, "response"))
    if err != nil { return nil, err }
    response_document := etree.NewDocument()
    _, err = response_document.ReadFrom(
        charmap.ISO8859_1.NewDecoder().Reader(response_file),
    )
    if err != nil { return nil, err }
    response_document.Root().CreateAttr(MESSAGE_ID, message_id)
    if err != nil { return nil, err }
    response_bytes_utf8, err := response_document.WriteToBytes()
    if err != nil { return nil, err }
    response_bytes_iso8859_1, err := charmap.ISO8859_1.NewEncoder().Bytes(response_bytes_utf8)
    return response_bytes_iso8859_1, nil
}

func handle(conn net.Conn) {
    defer conn.Close()
    // get incoming HTTP request from connection and construct etree.Document
    request, err := http.ReadRequest(bufio.NewReader(conn))
    if err != nil { log.Fatal(err) }
    request_document := etree.NewDocument()
    _, err = request_document.ReadFrom(request.Body)
    if err != nil { log.Fatal(err) }

    // extract message identifier from etree.Document
    message_id := request_document.Root().SelectAttr(MESSAGE_ID).Value

    // get bytes from etree.Document
    request_bytes, err := request_document.WriteToBytes()
    if err != nil { log.Fatal(err) }

    // lock and unlock mutex for `requests` map accesss
    mutex.Lock()
    defer mutex.Unlock()

    // match incoming request with known request
    for index_name, known_request_document := range requests {
        // write incoming message identifier to known request message id
        known_request_document.Root().CreateAttr(MESSAGE_ID, message_id)
        known_request_bytes, err := known_request_document.WriteToBytes()
        if err != nil { log.Fatal(err) }
        if bytes.Equal(request_bytes, known_request_bytes) {
            response_bytes, err := get_response_bytes(index_name, message_id)
            if err != nil { log.Fatal(err) }
            _, err = io.Copy(conn, bytes.NewReader(response_bytes))
            if err != nil { log.Fatal(err) }
            log.Printf("<- %s\n", index_name)
            return
        }
    }
    log.Println("<- X")

    // we didn’t match that request, let’s debug it
    debug_path := path.Join(workdir, "debug")
    debug_dump, err := os.Create(debug_path)
    defer debug_dump.Close()
    if err != nil { log.Fatal(err) }
    _, err = debug_dump.Write(request_bytes)
    if err != nil { log.Fatal(err) }
    log.Println(string(request_bytes))

    log.Fatal("failed to match")
}

func main() {
  log.SetFlags(log.Ltime | log.Lshortfile)

  // get required vars from environment
  listen, ok := os.LookupEnv("LISTEN")
  if !ok { panic("set LISTEN") }
  workdir, ok = os.LookupEnv("WORKDIR")
  if !ok { log.Fatal("set WORKDIR") }

  // list the request/response pairs stored in `workdir`
  files, err := ioutil.ReadDir(workdir)
  if err != nil { log.Fatal(err) }

  // memory storage for known requests
  requests = make(map[string]*etree.Document, len(files))

  for _, index := range files {
    if !index.IsDir() { continue }
    index_name := index.Name()

    // construct an in-memory etree.Document from the body of an on-disk HTTP
    // request
    request_file, err := os.Open(path.Join(workdir, index_name, "request"))
    if err != nil { log.Fatal(err) }
    request, err := http.ReadRequest(bufio.NewReader(request_file))
    if err != nil { log.Fatal(err) }
    request_document := etree.NewDocument()
    _, err = request_document.ReadFrom(request.Body)
    if err != nil { log.Fatal(err) }
    requests[index_name] = request_document

  }
  
  server, server_err := net.Listen("tcp", listen)
  log.Printf("<- %s (%s)", workdir, listen)
  if server_err != nil { log.Fatal(server_err) }

  for {
    conn, err := server.Accept()
    if err != nil { log.Fatal(err) }
    go handle(conn)
  }
}
