.PHONY: test
DOCKER_COMPOSE=docker-compose --project-directory .
IMAGE_NAME=halfttp-mitm
GIT_SHA=$(shell git rev-parse --short HEAD)

define DOCKER_COMPOSE_DEVELOP
	$(DOCKER_COMPOSE) \
		-f docker/develop/compose.yml
endef

define DOCKER_COMPOSE_TEST
       $(DOCKER_COMPOSE) \
               -f docker/develop/compose.yml \
               -f docker/develop/test.yml
endef

define DOCKER_COMPOSE_CI_TEST
	$(DOCKER_COMPOSE) \
		-f docker/develop/compose.yml \
		-f docker/develop/test.yml \
		-f ci/compose.yml
endef

up-daemon:
	$(DOCKER_COMPOSE_DEVELOP) build
	$(DOCKER_COMPOSE_DEVELOP) up -d  # --force-recreate

develop-write: up-daemon
	$(DOCKER_COMPOSE_DEVELOP) exec mitm-write ash

develop-read: up-daemon
	$(DOCKER_COMPOSE_DEVELOP) exec mitm-read ash

request-write:
	$(DOCKER_COMPOSE_DEVELOP) exec requester make write

request-read:
	$(DOCKER_COMPOSE_DEVELOP) exec requester make read

logs:
	$(DOCKER_COMPOSE_DEVELOP) logs

down:
	$(DOCKER_COMPOSE_DEVELOP) down

test-build:
	$(DOCKER_COMPOSE_TEST) build

test-write:
	rm -rf test/out/*
	$(DOCKER_COMPOSE_TEST) run --rm requester make write

test-read:
	$(DOCKER_COMPOSE_TEST) run --rm requester make read

test-logs:
	$(DOCKER_COMPOSE_TEST) logs

test-down:
	$(DOCKER_COMPOSE_TEST) down

response:
	$(DOCKER_COMPOSE_DEVELOP) up -d --force-recreate responder
	$(DOCKER_COMPOSE_DEVELOP) exec responder ash

ps:
	$(DOCKER_COMPOSE_DEVELOP) ps

build:
	docker build -t $(IMAGE_NAME)-read \
		--build-arg app=read --target release .
	docker build -t $(IMAGE_NAME)-write \
		--build-arg app=write --target release .

push: build
	docker tag $(IMAGE_NAME)-read steadysupply/$(IMAGE_NAME)-read:$(GIT_SHA)
	docker tag $(IMAGE_NAME)-read steadysupply/$(IMAGE_NAME)-read:latest
	docker push steadysupply/$(IMAGE_NAME)-read:$(GIT_SHA)
	docker push steadysupply/$(IMAGE_NAME)-read:latest

	docker tag $(IMAGE_NAME)-write steadysupply/$(IMAGE_NAME)-write:$(GIT_SHA)
	docker tag $(IMAGE_NAME)-write steadysupply/$(IMAGE_NAME)-write:latest
	docker push steadysupply/$(IMAGE_NAME)-write:$(GIT_SHA)
	docker push steadysupply/$(IMAGE_NAME)-write:latest

ci-test:
	$(DOCKER_COMPOSE_CI_TEST) pull
	$(DOCKER_COMPOSE_CI_TEST) build
	$(DOCKER_COMPOSE_CI_TEST) push
	$(DOCKER_COMPOSE_CI_TEST) run --rm requester make write
	$(DOCKER_COMPOSE_CI_TEST) run --rm requester make read
