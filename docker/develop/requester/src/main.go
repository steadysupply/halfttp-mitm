// requester

package main

import (
    "os"
    "flag"
    "fmt"
    "io/ioutil"
    "log"
    "net"
    "net/http"
    "net/http/httputil"
    "strings"
)

func main () {
    log.SetFlags(log.Ltime | log.Lshortfile)
    target := flag.String("target", "", "<host>:<port>"); flag.Parse()
    if *target == "" {
      log.Fatal("pass -target <host>:<port>")
    }
    info, err := os.Stdin.Stat()
    if err != nil { log.Fatal(err) }
    if (info.Mode() & os.ModeCharDevice) != 0 {
				log.Fatal("pipe in the request body")
    }
    body, err := ioutil.ReadAll(os.Stdin)
    if err != nil { log.Fatal(err) }

    log.Printf("-> %s\n", *target)

    request, err := http.NewRequest(
      "IGNORED", fmt.Sprintf("http://%s", *target),
      strings.NewReader(string(body)),
    )
    if err != nil { log.Fatal(err) }
    request_dump, err := httputil.DumpRequestOut(request, true)
    if err != nil { log.Fatal(err) }
    log.Println(string(request_dump))
    log.Println("sending")

    tcp_conn, err := net.Dial("tcp", *target)
    if err != nil { log.Fatal(err) }
    _, err = tcp_conn.Write(request_dump)
    log.Println("sent")
    if err != nil { log.Fatal(err) }

    log.Println("receiving")
    buf, err := ioutil.ReadAll(tcp_conn)
    if err != nil { log.Fatal(err) }
    log.Printf("received %s\n", buf)
    tcp_conn.Close()
}
