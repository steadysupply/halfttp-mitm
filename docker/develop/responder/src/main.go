// responder

package main

import (
    "os"
    "io"
    "bufio"
    "log"
    "net"
    "net/http"
)

var response = `<?xml version="1.0" encoding="UTF-8"?><bs_response messageId="46ef3ZA7imqvFNgfo_qSpYgv5Ak7RhyqsS_aetozas5n"><response><bsbaselist_response><neptune_status>ok</neptune_status><neptune_message>-</neptune_message><product_list><product><name code="ENG">England</name><region_list><region><name code="TH">Thames</name><base_list><base code="BEN">Benson</base><base code="CHER">Chertsey</base></base_list></region></region_list></product><product><name code="FRA">France</name><region_list><region><name code="AL">Alsace</name><base_list><base code="BOF">Boofzheim</base><base code="HES">Hesse</base></base_list></region><region><name code="BU">Bourgogne</name><base_list><base code="BRA">Branges</base><base code="FLC">Fontenoy le Chateau</base><base code="SJL">St Jean de Losne</base></base_list></region><region><name code="MICA">Midi / Camargue</name><base_list><base code="CAS">Castelnaudary</base><base code="HOM">Homps</base><base code="NAR">Narbonne</base><base code="PCA">Port Cassafieres</base><base code="STG">St Gilles</base><base code="TRE">Trebes</base></base_list></region><region><name code="LONI">Loire / Nivernais</name><base_list><base code="CSL">Chatillon Sur Loire</base><base code="DEC">Decize</base><base code="MIG">Migennes</base><base code="TNN">Tannay</base></base_list></region><region><name code="AQ">Aquitaine</name><base_list><base code="CTL">Castelsarrasin</base><base code="MAS">Le Mas d'Agenais</base></base_list></region><region><name code="BR">Bretagne</name><base_list><base code="DIN">Dinan</base><base code="MES">Messac</base></base_list></region><region><name code="LT">Lot</name><base_list><base code="DOU">Douelle</base></base_list></region><region><name code="CR">Charente</name><base_list><base code="JAR">Jarnac</base></base_list></region></region_list></product><product><name code="IRE">Ireland</name><region_list><region><name code="IRE">Ireland</name><base_list><base code="CARR">Carrick-on-Shannon</base><base code="POR">Portumna</base></base_list></region></region_list></product><product><name code="ITA">Italy</name><region_list><region><name code="VE">Venice</name><base_list><base code="CSR">Casale sul Sile</base><base code="PRE">Precenicco</base></base_list></region></region_list></product><product><name code="HOL">Holland</name><region_list><region><name code="HO">Holland</name><base_list><base code="HIN">Hindeloopen</base><base code="VIN">Vinkeveen</base></base_list></region></region_list></product><product><name code="GER">Germany</name><region_list><region><name code="BD">Brandenburg</name><base_list><base code="JAB">Jabel</base><base code="MAR">Marina Wolfsbruch</base><base code="POTS">Potsdam</base></base_list></region></region_list></product><product><name code="SCO">Scotland</name><region_list><region><name code="SC">Scotland</name><base_list><base code="LAG">Laggan</base><base code="WHS">WHS - Laggan</base></base_list></region></region_list></product><product><name code="BEL">Belgium</name><region_list><region><name code="VL">Vlaanderen</name><base_list><base code="NPT">Nieuwpoort</base></base_list></region></region_list></product><product><name code="CAN">Canada</name><region_list><region><name code="CAN">Canada</name><base_list><base code="SEEL">Seeleys Bay</base><base code="SMTH">Smiths Falls</base></base_list></region></region_list></product></product_list></bsbaselist_response></response></bs_response>`

func handle(tcp_conn net.Conn) {
    defer tcp_conn.Close()
    _, err := http.ReadRequest(bufio.NewReader(tcp_conn))
    switch err {
    case nil:
    case io.EOF:
        // do nothing
    default:
        log.Fatal(err)
    }
    _, err = tcp_conn.Write([]byte(response))
    if err != nil { log.Fatal(err) }
}

func main() {
    log.SetFlags(log.Ltime | log.Lshortfile)
    listen, ok := os.LookupEnv("LISTEN")
    if !ok { panic("set LISTEN") }
    server, err := net.Listen("tcp", listen)
    if err != nil { log.Fatal(err) }
    log.Printf("listening on %s", listen)
    for {
        tcp_conn, err := server.Accept()
        if err != nil { log.Fatal(err) }
        go handle(tcp_conn)
    }
}
