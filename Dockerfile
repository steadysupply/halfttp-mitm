FROM golang:1.13.1-alpine AS develop
ARG app
ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

WORKDIR /go/src/app
RUN apk add git make gcc musl-dev
RUN go get -u github.com/go-delve/delve/cmd/dlv
COPY src/${app}/go.mod /go/src/app/
RUN go mod download
COPY src/${app}/ /go/src/app/
COPY docker/develop/wait /opt/wait
CMD ["/opt/wait"]

# ---

FROM develop AS build
RUN CGO_ENABLED=0 \
    go build \
    -a -installsuffix cgo \
    -ldflags '-w -s -extldflags "-static"' \
    -o /go/bin/app

# ---

FROM gcr.io/distroless/static as release
ARG app
COPY --from=build --chown=root /go/bin/app /app
CMD ["/app"]
